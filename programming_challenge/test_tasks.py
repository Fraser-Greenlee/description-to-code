import datetime
import time

from tasks import *
# from tasks_completed import *

# Tutorial

def test_todays_date():
    assert todays_date() == datetime.date.today()

def test_next_week():
    assert next_week() == datetime.date.today() + datetime.timedelta(weeks=3)

def test_n_seconds():
    n = 100 * 1000
    assert n_seconds(n) == datetime.date.today() + datetime.timedelta(seconds=n)



def _now(days=1):
    return datetime.datetime(10, 1, days)

def _today(days=1):
    return datetime.date(10, 1, days)

def _time(seconds=0):
    return datetime.time(0, seconds)


# Start

def test_add_days():
    assert add_days(_now(), 6) == _now(7)

def test_datetime_difference():
    a = _now()
    b = _now() + datetime.timedelta(days=13)
    assert datetime_difference(a, b) == 13

def test_datetime_in_weeks_and_days():
    assert datetime_in_weeks_and_days(3, 7, _now()) == _now(29)

def test_name_of_month_next_week():
    assert name_of_month_next_week(_now()) == 'January'

def test_mod_datetime_with_n():
    n = 100 * 1000
    assert mod_datetime_with_n(_now(), n) == (10, 'Tuesday')

def test_seconds_from_midnight_to_timestamp():
    assert seconds_from_midnight_to_timestamp(1) == 1

def test_time_deltas():
    a = datetime.datetime.now() - datetime.timedelta(weeks=2, days=4, seconds=18)
    b = _now()
    b_mod = b + datetime.timedelta(weeks=2, days=4)
    assert (b_mod - a).total_seconds() == time_deltas(a, b)

def test_remove_max_min():
    arr = [2,1,5,3,4,9,7,8,6]
    result = [2,5,3,4,6]
    assert set(result) == set(remove_max_min(arr))

def test_sums():
    arr =       [1,2,3, 4, 5, 6, 7,  8,9,  0]
    result =    [6,8,10,12,14,16,18,       4]
    assert set(result) == set(sums(arr))

def test_is_half_palindrome():
    arr = [
        2,5,8,0,9,9,
        6,4,2,8,6
    ]
    assert is_half_palindrome(arr) == False
    assert is_half_palindrome([1,3,4,7, 1,3,3,1]) == True

def test_check_only_even():
    arr = [2,4,7,3,5,7,9,2,4,6,4]
    assert check_only_even(arr) == False
    assert check_only_even([2,4,8,6,0,10,12,50]) == True
    arr =  [6,12,13,4,15,2,8,17,6,19,19,4]
    bad_arr =  [6,12,13,4, 1, 15,2,8,17,6,19,19,4]
    assert check_only_even(bad_arr) == False
    assert check_only_even(arr) == True
