
# desc2code User Study Info Sheet

The desc2code extension converts your descriptions of code (e.g. "todays date") into Python code (e.g. `datetime.date.today()`).

This study will test the usability of the extension by having YOU try to complete some small programming problems using it!

## How to use the extension

### Descriptions

Open a webpage with datetime module documentation.

Instead of writing code by hand with this tool you can describe the code you want in natural language. `"todays date" -> "datetime.date.today()"`

You can reference local and global variables while using the tool `"times a by b" -> "a*b"`

Descriptions only take letters and numbers except when part of a variable name.

The tool doesn't know your context so make variable types that you want clear. (e.g. NO `add n to mydate`, YES `add n days to mydate`)

If you are stuck on a question please try a few variations on your description, if that does not work you can forfit them where I will enter the correct answer.

### Model

1. Press `shift+cmd+p` and then type in "d2c" and you should be able to select "New d2c description", select this.
2. When prompted enter your new description.
3. One of these options will occur:
    1. It works, you will be prompted to make edits to the created code. Edit occordingly then press `Enter`.
    2. It doesn't work, you will have to write the code yourself then press `Enter`.
4. The code will be written to your editor and you can start on the next line.

(How an error message behaves will be shown to you.)

### Search

1. Press `shift+cmd+p` and then type in "d2c" and you should be able to select "New d2c description", select this.
2. When prompted enter your new description.
3. You will then see a selection of lines of code to choose from. One of these should be your correct peice of code. If none are press `esc`. If one is, select it and press `Enter`.
4. If it works you will be prompted to make edits to the created code. Edit occordingly then press `Enter`.
5. The code will be written to your editor and you can start on the next line.

## Your task

Your task is to complete the functions in `task.py` using the desc2code extension where possible.

As you complete the functions use `task_tests.py` to check your implimentations are correct.

Try to complete all the functions in 10 minutes!

Run tests using: `python -m pytest -x -q --tb=no`

DO NOT OPEN `test_tasks.py`

Tests should be done IN ORDER.
