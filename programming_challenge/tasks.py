import datetime


# Tutorial

# 1
def todays_date():
    '''
    Get todays date.

    Args:

    Returns:
        (datetime.date)
    '''


# 2
def next_week():
    '''
    Get the date 3 weeks from now.

    Args:

    Returns:
        (datetime.date)
    '''


# 3
def n_seconds(n):
    '''
    Get the date 'n' seconds from now.

    Args:
        n (int)

    Returns:
        (datetime.date)
    '''


# Start


# 4
def add_days(date, n):
    '''
    Get 'date' after 'n' days.

    Args:
        date (datetime.date)
        n (int)

    Returns:
        (datetime.date)
    '''


# 5
def datetime_difference(a, b):
    '''
    Get number of days from 'a' to 'b'.

    Args:
        a (datetime.date)
        b (datetime.date)

    Returns:
        (int) number of days
    '''



# 6
def datetime_in_weeks_and_days(a, b, date):
    '''
    Add `a` weeks and `b` days to `date`.

    Args:
        a (int)
        b (int)
        date (datetime.date)

    Returns:
        (datetime.date)
    '''



# 7
def name_of_month_next_week(date):
    """
    Get the name of the month a week after 'date'.

    Args:
        date (datetime.date)

    Returns:
        (str) name of the month
    """



# 8
def mod_datetime_with_n(d, n):
    '''
    Make a new datetime from datetime.date `d`.
    Add 3 days and `n` seconds to it.
    Return the new datetime's year and weekday name in a tuple (year, weekday).

    Args:
        d (datetime.date)
        n (int)
    Returns:
        (tuple) ((int) year number, (str) weekday name)
    '''



# 9
def seconds_from_midnight_to_timestamp(x):
    '''
    Create a new datetime from utc timestamp `x`.
    Get the number of seconds from the last midnight to that datetime.

    Args:
        x (int) a utc timestamp

    Returns:
        (int) number of seconds to midnight
    '''



# 10
def time_deltas(a, b):
    '''
    Find the days passed from datetime 'a' to now.
    Add that found number of days to datetime 'b'.
    Get the number of seconds from 'a' to the modified 'b'.

    Args:
        a (datetime.date)
        b (datetime.date)

    Returns:
        (int) number of seconds between 'a' and modified 'b'
    '''



# 11
def remove_max_min(arr):
    '''
    Remove the 3 biggest values and the smallest value in 'arr'.

    Args:
        arr (list)

    Returns:
        (list) arr with the 3 biggest values and smallest value removed
    '''



# 12
def sums(arr):
    '''
    Double each value in 'arr'.
    Remove the 2 largest values.
    Add 4 to each element.

    Args:
        arr (list)

    Returns:
        (list) modified 'arr'
    '''



# 13
def is_half_palindrome(arr):
    '''
    Get the last half of 'arr'.
    Return True if 'arr' is a palindrome, False otherwise.

    Args:
        arr (list)

    Returns:
        (boolean) if last half of 'arr' is a palindrome
    '''



# 14
def check_only_even(arr):
    '''
    Order arr's items in ascending order.
    Then check the first half only contains even numbers.

    Args:
        arr (list) list of numbers

    Returns:
        (boolean) if the halved 'arr' only contains even numbers
    '''

