# Challenge
Make a web app with flask to browse and modify a JSON file of pages.
- Users should be able to browse links to pages with the title of each page as the link.
- By going to the url `pages/<name\>` a page showing the contents should be displayed.
- Users should be able to add a new page by filling in a web form at `new_page`, having the details POSTed to a form adding the page to `pages.json` and then be headered to that new page.
- Should be able to search the pages by name through a simple GET request.

## Explanation
1. Require the coder to read up a page on Flask a day/week before!
2. Have the coder go through a small tutorial at first to see how to use it.
3. Then in the project:
   * Have repetition of cookie cutter code from defining multiple endpoints.
   * Needs to call a range of API calls multiple times that are hard to remember.
   * The call to render templates is kinda hard to remember.
   * Basic "framework" for the app is simple and easy to understand.

## What data I should generate?
* Code for modules Flask, json and Jinja as well as regular Python.

# Brainstorm Bellow


# What should the nature of the programming challenge be?
* Should (by it's nature) encourage use of the extension.
  * Requires some cookie-cutter code.
  * Needs some API calls of which the nature is known but not the implementation
* Must use Python.
* Must be short, 15m-30m.
* Should requrie repetition to encourage extension use.
* Must not be easier to Google the solution then to use the extension.


## Ideas

### Flask App
1. Require the coder to read up a page on Flask a day/week before!
2. Have the coder go through a small tutorial at first to see how to use it.
3. Then in the project:
   * Have repetition of cookie cutter code from defining multiple endpoints.
   * Needs to call a range of API calls multiple times that are hard to remember.
   * The call to render templates is kinda hard to remember.
   * Basic "framework" for the app is simple and easy to understand.
#### Challenge
Make a web browser for a JSON file of pages.
- Users should be able to browse links to pages with the title of each page as the link.
- By going to the url `pages/<name\>` a page showing the contents should be displayed.
- Users should be able to add a new page by filling in a web form at `new_page`, having the details POSTed to a form adding the page to `pages.json` and then be headered to that new page.
- Should be able to search the pages by name through a simple GET request.
