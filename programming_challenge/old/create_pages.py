"""
Script that creates a json file of pages for the programming problem.
"""
import names
import random
import json


NUMBER_OF_PAGES = 100


def _format_name(full_name):
    return full_name.lower().replace(' ', '_')

def _new_page(full_name):
    name = _format_name(full_name)
    age = str(random.randint(18,55))
    return {
        'name': name,
        'message': """Name: {0}, Age: {1}""".format(name, age)
    }

def _check_pages(pages):
    set([page['name'] for page in pages])

def _save_pages(pages):
    file = open('programming_challenge/pages.json', 'w')
    json.dump(pages, file)
    file.close()

def _generate_pages():
    pages = []
    for i in range(NUMBER_OF_PAGES):
        pages.append(
            _new_page(names.get_full_name())
        )

    _check_pages(pages)
    _save_pages(pages)


if __name__ == '__main__':
    _generate_pages()