import datetime

'''
# Before Starting

Try inputing a description of random characters.

This will show you an error message.
'''


# Tutorial

# 1
def todays_date():
    '''
    Get todays date.

    Use Description: "todays date"

    Args:

    Returns:
        (datetime.date)
    '''
    return datetime.date.today()


# 2
def next_week():
    '''
    Get the date 3 weeks from now.

    Use Description: "date in 3 weeks"

    Args:

    Returns:
        (datetime.date)
    '''
    return datetime.date.today() + datetime.timedelta(weeks=3)


# 3
def n_seconds(n):
    '''
    Get the date 'n' seconds from now.

    Use Description: "date n seconds from now"

    Args:
        n (int)

    Returns:
        (datetime.date)
    '''
    return datetime.date.today() + datetime.timedelta(seconds=n)


# Start


# 4
def add_days(date, n):
    '''
    Get 'date' after 'n' days.

    Args:
        date (datetime.date)
        n (int)

    Returns:
        (datetime.date)
    '''
    return date + datetime.timedelta(days=n)


# 5
def datetime_difference(a, b):
    '''
    Get number of days from 'a' to 'b'.

    Args:
        a (datetime.date)
        b (datetime.date)

    Returns:
        (int) number of days
    '''
    # forfit
    return (b - a).days



# 6
def datetime_in_weeks_and_days(a, b, date):
    '''
    Add `a` weeks and `b` days to `date`.

    Args:
        a (int)
        b (int)
        date (datetime.date)

    Returns:
        (datetime.date)
    '''
    return date + datetime.timedelta(weeks=a) + datetime.timedelta(days=b)





# 7
def name_of_month_next_week(date):
    """
    Get the name of the month a week after 'date'.

    Args:
        date (datetime.date)

    Returns:
        (str) name of the month
    """
    return (date + datetime.timedelta(weeks=1)).strftime("%B")




# 8
def mod_datetime_with_n(d, n):
    '''
    Make a new datetime from datetime.date `d`.
    Add 3 days and `n` seconds to it.
    Return the new datetime's year and weekday name in a tuple (year, weekday).

    Args:
        d (datetime.date)
        n (int)
    Returns:
        (tuple) ((int) year number, (str) weekday name)
    '''
    date = d + datetime.timedelta(days=3) + datetime.timedelta(seconds=n)
    return (date.year, date.strftime("%A"))



# 9
def seconds_from_midnight_to_timestamp(x):
    '''
    Create a new datetime from utc timestamp `x`.
    Get the number of seconds from the last midnight to that datetime.

    Args:
        x (int) a utc timestamp

    Returns:
        (int) number of seconds to midnight
    '''
    # forfit
    dt = datetime.datetime.utcfromtimestamp(x)
    result = (dt - dt.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
    return result



# 10
def time_deltas(a, b):
    '''
    Find the days passed from datetime 'a' to now.
    Add that found number of days to datetime 'b'.
    Get the number of seconds from 'a' to the modified 'b'.

    Args:
        a (datetime.date)
        b (datetime.date)

    Returns:
        (int) number of seconds between 'a' and modified 'b'
    '''
    days_passed = (datetime.datetime.now() - a).days
    b += datetime.timedelta(days=days_passed)
    return (b - a).total_seconds()



# 11
def remove_max_min(arr):
    '''
    Remove the 3 biggest values and the smallest value in 'arr'.

    Args:
        arr (list)

    Returns:
        (list) arr with the 3 biggest values and smallest value removed
    '''
    # forfit
    arr = sorted(arr, reverse=True)[3:]
    arr.remove(min(arr))
    return arr


# 12
def sums(arr):
    '''
    Double each value in 'arr'.
    Remove the 2 largest values.
    Add 4 to each element.

    Args:
        arr (list)

    Returns:
        (list) modified 'arr'
    '''
    arr = [v * 2 for v in arr]
    arr = sorted(arr, reverse=True)[2:]
    return [v + 4 for v in arr]



# 13
def is_half_palindrome(arr):
    '''
    Get the last half of 'arr'.
    Return True if 'arr' is a palindrome, False otherwise.

    Args:
        arr (list)

    Returns:
        (boolean) if last half of 'arr' is a palindrome
    '''
    # forfit



# 14
def check_only_even(arr):
    '''
    Order arr's items in ascending order.
    Then check the first half only contains even numbers.

    Args:
        arr (list) list of numbers

    Returns:
        (boolean) if the halved 'arr' only contains even numbers
    '''
    arr = sorted(arr)
    # forfit


