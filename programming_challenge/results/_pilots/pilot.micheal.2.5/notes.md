# ID user study notes

- Didn't return values, error checking on variable finder
- Check using datetime.date works
- Add repeat "plus", "subtract", etc terms on "and"
- When code is wrong it's hard to understand
- Improve wording on # 8
- Combos don't seem to ever work
- Code formatter gives errors
- Replace words of numbers with numbers
- Combo sort remove min & max
- Still sometimes doesn't return anything, some error?
- Do palindrome without the word palindrome
