model error "get time from datetime VARIABLE_0"
model error "get time from VARIABLE_0"
. param error, seems to have datetime as a variable
.
.
.
.
.
.
user error "VARIABLE_0 weeks in days"
model error "return VARIABLE_0 plus VARIABLE_1 weeks plus VARIABLE_2 days"
. parameterizer error?
. parameterizer error? also user error
.
.
user error "name of month plus week"
.
user error '+' token filtered out
parameterizer error? "n days ahead of VARIABLE_1"
.

(Queries found by line number)

Overall: 13/20 = 65% Accuracy

Error Type Breakdown:
    model error:        3/20    15%
    user error:         3/20    15%
    param/filter error: 1/20    5%

Thoughts
- Most went wrong because of the parameterizer+user not the model.
- User clearly very effected by when the model produced wrong code as he used much simpler snippets thereafter.
- Need to check predictions are not being lost/deleted.

