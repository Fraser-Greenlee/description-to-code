# Michael user study notes

Wants model to get context. Should get that 'n' is a number of days.

Often he asks the wrong thing

Is the model hard trained on these?

Model is often very confused by odd inputs.

User is often baffled when the model doesn't get it.

User quickly lowers expectations, only asking for single things in a command like language.

Didn't like the quality of the code produced.

Should be more clear on input types.

Users tend to copy the doc-strings

Often coders are baffled by docstrings as they don't know the underlying implementation.

Should show Param types in and types out

Tool becomes a puzzle slowing down the coder.



If it got it wrong it was often missing obvious context.

Also got combinations wrong, not returning anything.

A few of these are very badly formatted code

Didn't know what the question was asking

Would prefer a multi-step problem.

Would need 3-4 steps shouldn't allow typing in the docstring.

Didn't understand what was being created because he didn't know what he was asking.

Model would often produce garbage instead of saying "I don't understand".

Tool felt lazy and incurious. Sheep man.



(did not complete all questions, maybe half?)
