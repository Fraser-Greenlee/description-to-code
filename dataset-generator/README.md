# Dataset Generator

Generates a dataset of description-code pairs and stores them in MongoDB.

Runs on Python3.

Run using `generate_annotations.py`.


