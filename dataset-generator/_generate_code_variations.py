import random

from _render_basic_and_variation import render_basic_AND_all, render_basic_AND_random
from _render_complex_and_variation import render_complex_AND_all, render_complex_AND_random
from _render_code_variation import render_transform
from _code_param_index import new_param_index
from _code_return_index import new_return_index
from _code_transformers import (
    RETURN_TEMPLATES,
    PRINT_TEMPLATES,
    BASIC_AND_TEMPLATES
)
from _constants import (
    PRINT_VARIATIONS,
    RETURN_VARIATIONS
)


class CodeVariationGenerator:

    def __init__(self, description_variations):
        self.variations = [
            {
                'description_variations': variation[0],
                'code': variation[1],
                'modules': variation[2],
                'code_variations': []
            } for variation in description_variations
        ]
        self.param_type_index = new_param_index(self.variations)
        self.return_type_index = new_return_index(self.variations)

    def add_code_variations(self):
        '''
        Creates variations on the code snippets of pairs.

        Uses self.variations: [
            {
                'description_variations': [description,,,],
                'code': code,
                'modules': modules,
                'code_variations': []
            },,,
        ]
        And adds code variations: [
            {
                'description_variations': [description,,,],
                'code': code,
                'modules': modules,
                'code_variations': [{'description': description, 'code': code},,,]
            },,,
        ]
        '''
        new_variations = []
        for i, variation in enumerate(self.variations):
            description_variations = variation['description_variations']
            code = variation['code']
            return_types = self.return_type_index[code]
            code_variations = []
            if return_types:
                code_variations += render_transform(description_variations, code, RETURN_TEMPLATES, RETURN_VARIATIONS)
                code_variations += render_transform(description_variations, code, PRINT_TEMPLATES, PRINT_VARIATIONS)
                code_variations += self._render_complex_AND_transform(description_variations, code)
            code_variations += self._render_basic_AND_transform(description_variations, code)
            variation['code_variations'] = code_variations
            new_variations.append(variation)
        return new_variations

    def _render_complex_AND_transform(self, description_variations, code):
        combinable_variations = {p_type: self.param_type_index[p_type] for p_type in self.return_type_index[code]}
        if combinable_variations:
            n_combinable_variations = sum([len(combinable_variations[k]) for k in combinable_variations.keys()])
            if len(description_variations) <= n_combinable_variations:
                return render_complex_AND_all(description_variations, code, combinable_variations)
            else:
                return render_complex_AND_random(description_variations, code, combinable_variations)

    def _render_basic_AND_transform(self, description_variations, code):
        if len(description_variations) <= len(self.variations):
            return render_basic_AND_all(description_variations, code, self.variations)
        else:
            return render_basic_AND_random(description_variations, code, self.variations)
