
class Pair:
    """
    Define a new description-code pair.
    ** Assumes that description terms need a spaces prepended. **
    """
    def __init__(self, description, code, modules=[]):
        self.description = self._add_spaces(description)
        self.code = code
        self.modules = modules

    @staticmethod
    def _add_spaces(description):
        new_description = [description[0]]
        for terms in description[1:]:
            new_description.append(
                _space(terms)
            )
        return new_description


def _space(terms):
    return [
        ' ' + term for term in terms
    ]


def split(terms_string):
    return terms_string.split('|')


def optional(terms_string):
    split_options = split(terms_string)
    return [''] + split_options
