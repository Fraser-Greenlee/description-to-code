from _constants import (
    PARAMETER_PREFIXES,
    NUMBER_OF_EACH_PARAMETER_TYPE
)
PARAM_KEYS = []
for prefix in PARAMETER_PREFIXES:
    for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
        PARAM_KEYS.append(prefix + '_' + str(i))


def _param_name(key):
    return key[:key.index('_')]


def _find_param_type(code):
    param_type = None
    for key in PARAM_KEYS:
        if key in code:
            if param_type is None:
                param_type = _param_name(key)
            else:
                # if more than 1 param then return None
                return None
    return param_type


def new_param_index(description_variations):
    '''
    Makes an index of the param types of code snippets in description_variations.

    ONLY INDEXES DESCRIPTIONS WITH ONE PARAMETER

    index is a dict of the form: {
        param_type: [a_description_variation_dict,,,],,,
    }
    '''
    index = {}
    for variation in description_variations:
        code = variation['code']
        parameter_type = _find_param_type(code)
        if parameter_type:
            index[parameter_type] = index.get(parameter_type, []) + [variation]
    return index
