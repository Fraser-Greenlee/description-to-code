"""
Script to define description-code pairs
"""
from tqdm import tqdm
import json
import os
import pickle
import datetime as datetime_module
import pymongo
import random

from _constants import (
    MAX_VARIATIONS,
    DESCRIPTION_INDEX,
    CODE_INDEX
)
from _generate_code_variations import CodeVariationGenerator
from annotations import basic, datetime, arrays
ANNOTATION_MODULES = [basic, datetime, arrays]

NOW = datetime_module.datetime.now()
DB_CLIENT = pymongo.MongoClient("mongodb://localhost:27017/")
DB = DB_CLIENT['description_to_code']
IN_HOUSE_PAIRS = DB['generated_in_house']
IN_HOUSE_PAIRS_SEARCHABLE = DB['generated_in_house_searchable']


def _fix_permutation(perm):
    return perm.replace('   ', ' ').replace('  ', ' ').strip()


def _description_variations_from_a_pair(formula, code, modules):
    index = [0 for arr in formula]
    index[0] = -1
    fullIndex = [len(arr) - 1 for arr in formula]
    permutations = []
    while(index != fullIndex):
        for i in range(len(index)):
            if (index[i] < fullIndex[i]):
                index[i] += 1
                index[:i] = [0] * i
                break
        perm = ''
        for i in range(len(formula)):
            perm += str(formula[i][index[i]])
        perm = _fix_permutation(perm)
        if len(permutations) == MAX_VARIATIONS:
            permutations[random.randint(0, MAX_VARIATIONS - 1)] = perm
        elif len(permutations) < MAX_VARIATIONS:
            permutations.append(perm)
        else:
            raise Exception('Too long a permutation.')
    return [permutations, code, modules]


def _description_variations_from_pairs(description_code_pairs):
    rows = []
    for pair in tqdm(description_code_pairs):
        rows.append(
            _description_variations_from_a_pair(pair.description, pair.code, pair.modules)
        )
    return rows


def _add_code_variations(description_variations):
    variation_generator = CodeVariationGenerator(description_variations)
    return variation_generator.add_code_variations()


def _flatten_description_variation(a_variation):
    return [
        {
            'description': description_variation,
            'code': a_variation['code']
        } for description_variation in a_variation['description_variations']
    ]


def _random_selection(elements, max):
    random.shuffle(elements)
    return elements[:max]


def _flatten_variations(variations):
    flat_variations = []
    for a_variation in variations:
        its_variations = _flatten_description_variation(a_variation) + a_variation['code_variations']
        if len(its_variations) > MAX_VARIATIONS:
            flat_variations += _random_selection(its_variations, MAX_VARIATIONS)
        else:
            flat_variations += its_variations
    return flat_variations


def _save_variations(variations):
    for i, _ in enumerate(variations):
        variations[i]['created_at'] = NOW
    IN_HOUSE_PAIRS.insert_many(variations)


def _join_descriptions_for_code(descriptions):
    return ' | '.join(descriptions)


def _get_code_to_descriptions(variations):
    code_to_descriptions = dict()
    for a_variation in tqdm(variations):
        code = a_variation[CODE_INDEX]
        if code in code_to_descriptions:
            code_to_descriptions[code] += a_variation[DESCRIPTION_INDEX]
        else:
            code_to_descriptions[code] = a_variation[DESCRIPTION_INDEX]
    return code_to_descriptions


def _save_search_variations(variations):
    IN_HOUSE_PAIRS_SEARCHABLE.remove()
    all_code_to_descriptions = _get_code_to_descriptions(variations)
    new_pairs = []
    for code, descriptions in tqdm(all_code_to_descriptions.items()):
        pair = {
            'description': _join_descriptions_for_code(descriptions),
            'code': code,
            'created_at': NOW
        }
        new_pairs.append(pair)
    IN_HOUSE_PAIRS_SEARCHABLE.insert_many(new_pairs)


def generate_annotations_for_each_script():
    description_variations = []
    for module in ANNOTATION_MODULES:
        description_variations += _description_variations_from_pairs(module.description_code_pairs)
    all_variations = _add_code_variations(description_variations)
    flat_variations = _flatten_variations(all_variations)
    _save_variations(flat_variations)
    _save_search_variations(description_variations)
    return len(flat_variations)


if __name__ == '__main__':
    IN_HOUSE_PAIRS.remove({})
    total = generate_annotations_for_each_script()
    print('Total Pairs: ' + str(total))
