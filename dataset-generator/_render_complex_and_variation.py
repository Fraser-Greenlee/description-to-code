import random

from _code_transformers import (
    COMPLEX_AND_TEMPLATES
)
from _constants import (
    AND_COMPLEX_VARIATIONS
)


def render_complex_AND_all(description_variations, og_code, combinable_variations):
    new_variations = []
    for og_description in description_variations:
        new_variations.append(
            _new_variation(og_description, og_code, combinable_variations)
        )
    return new_variations


def render_complex_AND_random(description_variations, og_code, combinable_variations):
    new_variations = []
    for _ in range(AND_COMPLEX_VARIATIONS):
        og_description = random.choice(description_variations)
        new_variations.append(
            _new_variation(og_description, og_code, combinable_variations)
        )
    return new_variations


def _new_variation(og_description, og_code, combinable_variations):
    combine_description, combine_code = _new_combinable_variation(combinable_variations)
    raw_description = random.choice(COMPLEX_AND_TEMPLATES['description'])
    raw_code = COMPLEX_AND_TEMPLATES['code']
    return {
        'description': raw_description.format(og_description, combine_description),
        'code': raw_code.format(og_code, combine_code)
    }


def _new_combinable_variation(combinable_variations):
    p_type = random.choice(list(combinable_variations.keys()))
    p_replace = p_type + '_0'
    variation = random.choice(combinable_variations[p_type])
    description_raw = random.choice(variation['description_variations'])
    it_replace = random.choice(COMPLEX_AND_TEMPLATES['replace_params']['description'])
    description = description_raw.replace(p_replace, it_replace)
    code = variation['code'].replace(p_replace, COMPLEX_AND_TEMPLATES['replace_params']['code'])
    return description, code
