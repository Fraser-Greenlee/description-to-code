import random


def render_transform(description_variations, code, templates, max_n):
    if len(description_variations) <= max_n:
        return _render_all(description_variations, code, templates)
    return _render_random(description_variations, code, templates, max_n)


def _render_all(description_variations, og_code, templates):
    code_variations = []
    code = templates['code'].format(og_code)
    for og_description in description_variations:
        code_variations.append(
            _new_code_variation(og_description, code, templates)
        )
    return code_variations


def _render_random(description_variations, og_code, templates, max_n):
    code_variations = []
    code = templates['code'].format(og_code)
    for _ in range(max_n):
        og_description = random.choice(description_variations)
        code_variations.append(
            _new_code_variation(og_description, code, templates)
        )
    return code_variations


def _new_code_variation(og_description, code, templates):
    description = _format_random(templates['description'], og_description)
    return {
        'description': description,
        'code': code
    }


def _format_random(description_templates, description):
    return random.choice(description_templates).format(description)