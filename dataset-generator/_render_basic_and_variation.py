import random

from _code_transformers import (
    BASIC_AND_TEMPLATES
)
from _constants import (
    AND_BASIC_VARIATIONS
)


def render_basic_AND_all(description_variations, og_code, all_variations):
    new_variations = []
    for og_description in description_variations:
        new_variations.append(
            _new_variation(og_description, og_code, all_variations)
        )
    return new_variations


def render_basic_AND_random(description_variations, og_code, all_variations):
    new_variations = []
    for _ in range(AND_BASIC_VARIATIONS):
        og_description = random.choice(description_variations)
        new_variations.append(
            _new_variation(og_description, og_code, all_variations)
        )
    return new_variations


def _new_variation(og_description, og_code, all_variations):
    combine_variation = random.choice(all_variations)
    combine_description = random.choice(combine_variation['description_variations'])
    description_template = random.choice(BASIC_AND_TEMPLATES['description'])
    return {
        'description': description_template.format(og_description, combine_description),
        'code': BASIC_AND_TEMPLATES['code'].format(og_code, combine_variation['code'])
    }
