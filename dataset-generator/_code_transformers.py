
RETURN_TEMPLATES = {
    'description': [
        'return {0}',
        '{0} and return it',
        '{0} return it',
        '{0} and return',
        '{0} then return it',
        '{0} then return'
    ],
    'code': 'return {0}'
}


PRINT_TEMPLATES = {
    'description': [
        'print {0}',
        'log {0}',
        'output to console {0}',
        'output to the console {0}',
        '{0} and output it to the console',
        '{0} and print it',
        '{0} print it',
        '{0} and print',
        '{0} then print it',
        '{0} then print'
    ],
    'code': 'print({0})'
}


COMPLEX_AND_TEMPLATES = {
    'replace_params': {
        'description': [
            'it',
            'the value',
            'the item'
        ],
        'code': 'result'
    },
    'description': [
        '{0} and {1}',
        '{0} and then {1}',
        '{0} then {1}',
        '{0} after that {1}',
        '{0} afterwards {1}',
        '{0} {1}'
    ],
    'code': 'result = {0}\n{1}'
}


BASIC_AND_TEMPLATES = {
    'description': [
        '{0} and {1}',
        '{0} and then {1}',
        '{0} then {1}',
        '{0} after that {1}',
        '{0} afterwards {1}',
        '{0} {1}'
    ],
    'code': '{0}\n{1}'
}
