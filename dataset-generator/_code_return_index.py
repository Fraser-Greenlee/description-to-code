import numbers

from _constants import (
    P_STRING,
    P_NUMBER,
    P_LIST,
    P_DICT,
    P_SET,
    PARAMETER_PREFIXES,
    NUMBER_OF_EACH_PARAMETER_TYPE
)

PARAMETER_INPUTS = {
    'STRING': ["'example string'", "'abc'", "''", "u' '", "r' '", "',,,'"],
    'NUMBER': ['300', '1', '0', '-20', '-1', '0.5', '-1.5'],
    'LIST': ["['a', 'b', 'c']", "['ab', 'bc']", "[True, False, True]", "[2, 10, 4]", "[0, 0]", "[-1, -10, -2]", "[1,2,3,4,5,6,7,8,9,10,11,12,13,14]", "[]", "[[], []]", "[['s'], ['ab','a']]"],
    'DICT': ["{'a': 1, 'b': 3}", "{'a': True, 'b': False}", "{'a': 'c', 'b': 'd'}"],
    'SET': ["{1,3,2}", "{True, False}", "{'a','b','c'}"],
    'VARIABLE': []
}
for k in PARAMETER_PREFIXES:
    PARAMETER_INPUTS['VARIABLE'] += PARAMETER_INPUTS[k]


PARAMETER_KEYS = []
for prefix in PARAMETER_PREFIXES:
    for i in range(NUMBER_OF_EACH_PARAMETER_TYPE):
        PARAMETER_KEYS.append(prefix + '_' + str(i))


def _fix_permutation(perm):
    return perm.replace('   ', ' ').replace('  ', ' ').strip()


def _make_lists(formula):
    new_formula = []
    for item in formula:
        if type(item) is not list:
            new_formula.append(
                [item]
            )
        else:
            new_formula.append(item)
    return new_formula


def _code_variations(formula):
    formula = _make_lists(formula)
    index = [0 for arr in formula]
    index[0] = -1
    fullIndex = [len(arr) - 1 for arr in formula]
    permutations = []
    while(index != fullIndex):
        for i in range(len(index)):
            if (index[i] < fullIndex[i]):
                index[i] += 1
                index[:i] = [0] * i
                break
        perm = ''
        for i in range(len(formula)):
            perm += str(formula[i][index[i]])
        perm = _fix_permutation(perm)
        permutations.append(perm)
    return permutations


def _param_name(key):
    return key[:key.index('_')]


def _param_locations(code):
    locations = []
    for k in PARAMETER_KEYS:
        if k in code:
            locations.append({
                'param_type': _param_name(k),
                'starting_i': code.index(k),
                'length': len(k)
            })
    locations.sort(key=lambda x: x['starting_i'])
    return locations


def _code_options(code, locations):
    options = []
    i = 0
    for loc_i, _ in enumerate(locations):
        if i < locations[loc_i]['starting_i']:
            options.append(code[i:locations[loc_i]['starting_i']])
        options.append(PARAMETER_INPUTS[locations[loc_i]['param_type']])
        i = locations[loc_i]['starting_i'] + locations[loc_i]['length']
    options.append(code[i:])
    return options


def _get_code_options(code):
    param_locations = _param_locations(code)
    code_options = _code_options(code, param_locations)
    return code_options


def _find_return_types(code):
    code_options = _get_code_options(code)
    code_variations = _code_variations(code_options)
    types = set()
    for code in code_variations:
        try:
            types.add(type(eval(code)))
        except:
            pass
    return types


def _python_to_param_type(py_type):
    if py_type is str:
        return P_STRING
    if py_type in [int, float, complex]:
        return P_NUMBER
    if py_type is list:
        return P_LIST
    if py_type is dict:
        return P_DICT
    if py_type is set:
        return P_SET
    return None


def _python_types_to_param_types(python_types):
    param_types = set()
    for py_type in python_types:
        a_param_type = _python_to_param_type(py_type)
        if a_param_type:
            param_types.add(a_param_type)
    return list(param_types)


def new_return_index(description_variations):
    '''
    Makes an index of the return types of code snippets in description_variations.

    index is a dict of the form: {
        '{code}': {return_type,,,},
        {return_type}: [a_description_variation_dict,,,]
    }
    '''
    index = {}
    for variation in description_variations:
        code = variation['code']
        python_return_types = _find_return_types(code)
        return_types = _python_types_to_param_types(python_return_types)
        index[code] = return_types
        for a_type in return_types:
            index[a_type] = index.get(a_type, []) + [variation]
    return index
