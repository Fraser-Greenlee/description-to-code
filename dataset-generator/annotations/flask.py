from _annotation_tools import Pair, split, optional

description_code_pairs = [
    Pair(
        description=[
            split('route|on path'),
            ['STRING_0']
        ],
        code='''@app.route(STRING_0)
def function_name():
    ''',
        modules=['flask']
    ),
    Pair(
        description=[
            split('on|route'),
            ['STRING_0'],
            split('post|POST'),
            optional('request')
        ],
        code='''@app.route(STRING_0, methods=['POST'])
def function_name():
    ''',
        modules=['flask']
    ),
    Pair(
        description=[
            split('on|route'),
            ['STRING_0'],
            split('get|GET'),
            optional('request')
        ],
        code='''@app.route(STRING_0, methods=['GET'])
def function_name():
    ''',
        modules=['flask']
    ),
    Pair(
        description=[
            ['get'],
            optional('sent|posted|request'),
            ['form']
        ],
        code='requests.form',
        modules=['flask']
    ),

    Pair(
        description=[
            split('render|process'),
            optional('html|webpage'),
            ['STRING_0']
        ],
        code="render_template(STRING_0, param=value)",
        modules=['flask']
    ),
    Pair(
        description=[
            optional('flask'),
            ['debug'],
            ['VARIABLE_0']
        ],
        code="VARIABLE_0.run(debug=True)",
        modules=['flask']
    ),
    Pair(
        description=[
            optional('flask'),
            ['run'],
            ['VARIABLE_0']
        ],
        code="VARIABLE_0.run()",
        modules=['flask']
    )
]
