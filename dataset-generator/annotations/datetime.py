from _annotation_tools import Pair, split, optional

'''
Creates dataset that gives minimum possible coverge, demanding a generalizing model.

Generalizations I want.


Train
    "date in 2 days"
    "date in 3 weeks"
    "date in 4 days and 2 weeks"
    "date in 4 weeks and 2 days"
    "date in 2 seconds"

    "date 2 days away"

Test
    "date in 2 seconds and 3 days"

    "date 2 weeks away"



'''

LARGEST = split('largest|greatest|biggest|max|maximum')
SMALLEST = split('smallest|lowest|min|minimum')
GET = optional('get|find|take|finds|takes')
CURRENT = split('current|today|todays|right now|for now|now|at the moment|currently|present')
CURREN_OPT = optional('current|today|todays|right now|for now|now|at the moment|currently|present')
SUM_STR = 'sum|add|adds|sums|combines|additions|combine|join|plus|addition'
SUM = split(SUM_STR)
SUBTRACT = split('subtract|subtract|differences|difference|difference between|in between|amount between|minus|take away')
WEEKDAY = split('weekday|weekdays|day of the week|day name|name of the day of the week')
AFTER_STR = 'in|in the next|next|after|given|combine|join'
DURING = optional('on|of|during|at|from|with|via|using')
NUMBER = optional('number|count|amount|total|numbers|amounts|counts|int|item|unit|float|integer|ints|items|units|floats|integers|element|value|member|elements|values|members')
MIDNIGHT = split('midnight|12 pm|end of day|day ending|day end')
DATETIME = split('datetime|datetime datetime|a datetime|date time|a date time')
AWAY = optional('of time|away|passed|of time passed|time passed|from now|from today|from present|after today|after present|passed|later')
AWAY_NON_OPT = optional('of time|away|passed|of time passed|time passed|from now|from today|from present|after today|after present|passed|later')
AFTER = split('in|in the next|after|given|past|over|' + SUM_STR)

DATE_OR_DATETIME_VAR_0 = optional(
    'VARIABLE_0'
    + '|datetime VARIABLE_0|datetime datetime VARIABLE_0|a datetime VARIABLE_0|date time VARIABLE_0|date time date time VARIABLE_0|a date time VARIABLE_0|date VARIABLE_0|a date VARIABLE_0|datetime date VARIABLE_0|date time date VARIABLE_0'
    + '|VARIABLE_0 datetime|VARIABLE_0 datetime datetime|VARIABLE_0 a datetime|VARIABLE_0 date time|VARIABLE_0 date time date time|VARIABLE_0 a date time|VARIABLE_0 date|VARIABLE_0 a date|VARIABLE_0 datetime date|VARIABLE_0 date time date'
)
DATE_OR_DATETIME_VAR_1 = optional(
    'VARIABLE_1'
    + '|datetime VARIABLE_1|datetime datetime VARIABLE_1|a datetime VARIABLE_1|date time VARIABLE_1|date time date time VARIABLE_1|a date time VARIABLE_1|date VARIABLE_1|a date VARIABLE_1|datetime date VARIABLE_1|date time date VARIABLE_1'
    + '|VARIABLE_1 datetime|VARIABLE_1 datetime datetime|VARIABLE_1 a datetime|VARIABLE_1 date time|VARIABLE_1 date time date time|VARIABLE_1 a date time|VARIABLE_1 date|VARIABLE_1 a date|VARIABLE_1 datetime date|VARIABLE_1 date time date'
)


date_basic = [
    Pair(
        description=[
            split('return|returns'),
            CURRENT,
            split('date|dates')
        ],
        code='return datetime.date.today()'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            CURREN_OPT
        ],
        code='datetime.date.today()'
    ),
    Pair(
        description=[
            optional('the'),
            CURREN_OPT,
            split('date|dates')
        ],
        code='datetime.date.today()'
    ),
    Pair(
        description=[
            optional('the'),
            optional('todays|days'),
            split('date|dates')
        ],
        code='datetime.date.today()'
    ),
    Pair(
        description=[
            optional('the'),
            ['datetime'],
            CURREN_OPT
        ],
        code='datetime.datetime.now()'
    ),
    Pair(
        description=[
            optional('the'),
            CURREN_OPT,
            ['datetime']
        ],
        code='datetime.datetime.now()'
    ),
    Pair(
        description=[
            optional('the'),
            ['datetime'],
            optional('for'),
            optional('the'),
            split('today|current date|current day'),
        ],
        code='datetime.datetime.today()'
    )
]


date_testable_pairs = [
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('days NUMBER_0|day NUMBER_0|NUMBER_0 days|NUMBER_0 day'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(days=NUMBER_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            optional('a'),
            optional('one|single'),
            split('days|day'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(days=1)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            optional('a'),
            optional('one|single'),
            split('week|weeks'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(weeks=1)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            optional('a'),
            optional('one|single'),
            split('week|weeks'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(weeks=1)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('NUMBER_0 weeks|weeks NUMBER_0|NUMBER_0 week|week NUMBER_0'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('NUMBER_0 days|days NUMBER_0'),
            optional('and'),
            split('NUMBER_1 weeks|weeks NUMBER_1|NUMBER_1 week|week NUMBER_1'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(days=NUMBER_0, weeks=NUMBER_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 weeks|weeks VARIABLE_0|VARIABLE_0 week|week VARIABLE_0'),
            optional('and|with'),
            split('VARIABLE_1 days|days VARIABLE_1'),
            optional('from|after|past'),
            split('now|current|present')
        ],
        code='datetime.date.today() + datetime.timedelta(days=VARIABLE_1, weeks=VARIABLE_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('NUMBER_0 weeks|weeks NUMBER_0|NUMBER_0 week|week NUMBER_0'),
            optional('and'),
            split('NUMBER_1 days|days NUMBER_1'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(days=NUMBER_1, weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('NUMBER_0 seconds|seconds NUMBER_0'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(seconds=NUMBER_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            AFTER,
            split('VARIABLE_0 seconds|seconds VARIABLE_0'),
            AWAY
        ],
        code='datetime.date.today() + datetime.timedelta(seconds=VARIABLE_0)'
    ),
    Pair(
        description=[
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            split('VARIABLE_0 seconds|seconds VARIABLE_0'),
            split('from|before|coming to|to'),
            split('now|the present|present|current|current time')
        ],
        code='datetime.date.today() + datetime.timedelta(seconds=VARIABLE_0)'
    ),
    Pair(
        description=[
            SUM,
            ['NUMBER_0'],
            split('weeks|week'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            SUM,
            optional('a'),
            optional('one|single'),
            split('weeks|week'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=1)'
    ),
    Pair(
        description=[
            SUM,
            split('weeks VARIABLE_0|week VARIABLE_0|VARIABLE_0 week|VARIABLE_0 weeks'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 + datetime.timedelta(weeks=VARIABLE_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUM,
            split('weeks VARIABLE_1|week VARIABLE_1|VARIABLE_1 week|VARIABLE_1 weeks'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUBTRACT,
            split('weeks NUMBER_0|week NUMBER_0|NUMBER_0 week|NUMBER_0 weeks'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 - datetime.timedelta(weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUBTRACT,
            optional('a'),
            optional('one|single'),
            split('weeks|week'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 - datetime.timedelta(weeks=1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUBTRACT,
            split('weeks VARIABLE_1|week VARIABLE_1|VARIABLE_1 week|VARIABLE_1 weeks'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 - datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUBTRACT,
            split('weeks NUMBER_0|week NUMBER_0|NUMBER_0 week|NUMBER_0 weeks'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 - datetime.timedelta(weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            SUBTRACT,
            optional('a'),
            optional('one|single'),
            split('weeks|week'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0 - datetime.timedelta(weeks=1)'
    ),
    Pair(
        description=[
            SUBTRACT,
            split('weeks VARIABLE_0|week VARIABLE_0|VARIABLE_0 week|VARIABLE_0 weeks'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 - datetime.timedelta(weeks=VARIABLE_0)'
    ),
    Pair(
        description=[
            GET,
            DATE_OR_DATETIME_VAR_0,
            optional('and|and then'),
            SUM,
            ['NUMBER_0'],
            split('weeks|week'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            GET,
            DATE_OR_DATETIME_VAR_0,
            optional('and|and then'),
            SUM,
            ['VARIABLE_1'],
            split('weeks|week'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_0 days|days NUMBER_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_1 weeks|weeks NUMBER_1|NUMBER_1 week|week NUMBER_1'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=NUMBER_0, weeks=NUMBER_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 days|days VARIABLE_1'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_0 weeks|weeks NUMBER_0|NUMBER_0 week|week NUMBER_0'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=VARIABLE_1, weeks=NUMBER_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_0 days|days NUMBER_0|NUMBER_0 day|day NUMBER_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=NUMBER_0, weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            optional('and'),
            split('VARIABLE_2 days|days VARIABLE_2|VARIABLE_2 day|day VARIABLE_2'),
            AWAY_NON_OPT
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1, days=VARIABLE_2)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 days|days VARIABLE_0|VARIABLE_0 day|day VARIABLE_0'),
            optional('and'),
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            optional('to'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(days=VARIABLE_0, weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 weeks|weeks VARIABLE_0|VARIABLE_0 week|week VARIABLE_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
            optional('to'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(weeks=VARIABLE_0, days=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 seconds|seconds VARIABLE_0|VARIABLE_0 second|second VARIABLE_0'),
            optional('and'),
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
            optional('to'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(seconds=VARIABLE_0, days=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 seconds|seconds VARIABLE_0|VARIABLE_0 second|second VARIABLE_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            optional('to'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(seconds=VARIABLE_0, weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_2 weeks|weeks VARIABLE_2|VARIABLE_2 week|week VARIABLE_2'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(seconds=VARIABLE_1, weeks=VARIABLE_2)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            optional('and'),
            split('VARIABLE_2 seconds|seconds VARIABLE_2|VARIABLE_2 second|second VARIABLE_2'),
            AWAY_NON_OPT
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1, seconds=VARIABLE_2)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_0 days|days NUMBER_0|NUMBER_0 day|day NUMBER_0'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(seconds=VARIABLE_1, days=NUMBER_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional(AFTER_STR + SUM_STR),
            split('NUMBER_0 days|days NUMBER_0|NUMBER_0 day|day NUMBER_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=NUMBER_0, seconds=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_0 days|days VARIABLE_0|VARIABLE_0 day|day VARIABLE_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('to'),
            optional('date|datetime'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(days=VARIABLE_0, seconds=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_0 weeks|weeks VARIABLE_0|VARIABLE_0 week|week VARIABLE_0'),
            optional('and'),
            optional(AFTER_STR + SUM_STR),
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('to'),
            optional('date|datetime'),
            ['VARIABLE_2']
        ],
        code='VARIABLE_2 + datetime.timedelta(weeks=VARIABLE_0, seconds=VARIABLE_1)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            AFTER,
            ['NUMBER_0'],
            split('days|day'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=NUMBER_0)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            AFTER,
            split('NUMBER_0 seconds|seconds NUMBER_0'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(seconds=NUMBER_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('in|in the next|after|given|past|over|' + SUM_STR),
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(days=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
            AWAY_NON_OPT
        ],
        code='VARIABLE_0 + datetime.timedelta(days=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('in|in the next|after|given|past|over|' + SUM_STR),
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            AWAY
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
            AWAY_NON_OPT
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 days|days VARIABLE_0|VARIABLE_0 day|day VARIABLE_0'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 + datetime.timedelta(days=VARIABLE_0)'
    ),
    Pair(
        description=[
            split('VARIABLE_0 days|days VARIABLE_0|VARIABLE_0 day|day VARIABLE_0'),
            split('ahead|in front|after|over|past'),
            optional('of'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 + datetime.timedelta(days=VARIABLE_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUM,
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 + datetime.timedelta(days=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 seconds|seconds VARIABLE_0|VARIABLE_0 second|second VARIABLE_0'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 + datetime.timedelta(seconds=VARIABLE_0)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            SUM,
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('to|upon|with|and'),
            optional('it')
        ],
        code='VARIABLE_0 + datetime.timedelta(seconds=VARIABLE_1)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            SUM,
            split('VARIABLE_1 days|days VARIABLE_1|VARIABLE_1 day|day VARIABLE_1'),
        ],
        code='VARIABLE_0 + datetime.timedelta(days=VARIABLE_1)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            SUM,
            split('VARIABLE_1 weeks|weeks VARIABLE_1|VARIABLE_1 week|week VARIABLE_1'),
        ],
        code='VARIABLE_0 + datetime.timedelta(weeks=VARIABLE_1)'
    ),
    Pair(
        description=[
            SUM,
            split('VARIABLE_0 weeks|weeks VARIABLE_0|VARIABLE_0 week|week VARIABLE_0'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 + datetime.timedelta(weeks=VARIABLE_0)'
    ),
    Pair(
        description=[
            SUBTRACT,
            split('VARIABLE_0 days|days VARIABLE_0|VARIABLE_0 day|day VARIABLE_0'),
            optional('to|upon|with|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='VARIABLE_1 - datetime.timedelta(days=VARIABLE_0)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            AFTER,
            split('VARIABLE_1 seconds|seconds VARIABLE_1|VARIABLE_1 second|second VARIABLE_1'),
            optional('of time|away|passed|of time passed|time passed|time')
        ],
        code='VARIABLE_0 + datetime.timedelta(seconds=VARIABLE_1)'
    ),
]

attribute_pairs = [
    Pair(
        description=[
            SMALLEST,
            ['year']
        ],
        code='datetime.MINYEAR'
    ),
    Pair(
        description=[
            LARGEST,
            ['year']
        ],
        code='datetime.MAXYEAR'
    )
]

TIMEDELTAS = split('timedeltas|time deltas|differences in time|deltas in time|deltas of time|time durations|durations of time|durations|period|period of time')
TIMEDELTA_STR = 'timedelta|time delta|difference in time|delta in time|delta of time|time duration|duration of time|duration|period|period of time'
TIMEDELTA = optional(TIMEDELTA_STR)
TIMES = split('times|multiply')

timedelta_pairs = [
    Pair(
        description=[
            SUM,
            TIMEDELTAS,
            ['VARIABLE_0'],
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 + VARIABLE_1'
    ),
    Pair(
        description=[
            SUM,
            TIMEDELTA,
            ['VARIABLE_0'],
            ['and'],
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 + VARIABLE_1'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            split('plus|add|sum'),
            ['VARIABLE_1'],
        ],
        code='VARIABLE_0 + VARIABLE_1'
    ),
    Pair(
        description=[
            SUM,
            ['VARIABLE_0'],
            ['and'],
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 + VARIABLE_1'
    ),

    Pair(
        description=[
            SUBTRACT,
            TIMEDELTAS,
            ['VARIABLE_0'],
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 - VARIABLE_1'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            SUBTRACT,
            ['VARIABLE_1'],
        ],
        code='VARIABLE_0 - VARIABLE_1'
    ),
    Pair(
        description=[
            SUBTRACT,
            TIMEDELTA,
            ['VARIABLE_0'],
            split('and|by'),
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 - VARIABLE_1'
    ),
    Pair(
        description=[
            SUBTRACT,
            ['VARIABLE_0'],
            split('and|by'),
            ['VARIABLE_1']
        ],
        code='VARIABLE_0 - VARIABLE_1'
    ),

    Pair(
        description=[
            ['double'],
            optional(TIMEDELTA_STR),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0*2'
    ),
    Pair(
        description=[
            TIMES,
            optional(TIMEDELTA_STR),
            ['VARIABLE_0'],
            optional('by'),
            ['NUMBER_0']
        ],
        code='VARIABLE_0*NUMBER_0'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            split('times|multiply'),
            ['VARIABLE_1'],
        ],
        code='VARIABLE_0 * VARIABLE_1'
    ),

    Pair(
        description=[
            GET,
            optional('the'),
            split('total|number|amount|count'),
            optional('of'),
            ['seconds'],
            optional('in'),
            TIMEDELTA,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.total_seconds()'
    )
]

DATE = split('date|datetime date|date time date')
DATE_OPT = optional('date|datetime date|date time date')

date_attributes_pairs = [
    Pair(
        description=[
            GET,
            optional('the'),
            split('year|years'),
            NUMBER,
            DURING,
            DATE_OPT,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.year'
    ),
    Pair(
        description=[
            GET,
            DATE_OPT,
            ['VARIABLE_0'],
            split('year|years'),
            NUMBER
        ],
        code='VARIABLE_0.year'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('year|years'),
            NUMBER,
            split('and|with'),
            optional('the'),
            WEEKDAY,
            optional('of|on'),
            DATE_OPT,
            ['VARIABLE_0'],
            optional('tuple|in tuple|in a tuple')
        ],
        code='(VARIABLE_0.year, VARIABLE_0.strftime("%A"))'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('month|months'),
            split('and|with'),
            optional('the'),
            split('year|years'),
            optional('of|on'),
            DATE_OPT,
            ['VARIABLE_0'],
            optional('tuple|in tuple|in a tuple')
        ],
        code='(VARIABLE_0.month, VARIABLE_0.year)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            WEEKDAY,
            split('and|with'),
            optional('the'),
            split('day|days'),
            optional('of|on'),
            DATE_OPT,
            ['VARIABLE_0'],
            optional('tuple|in tuple|in a tuple')
        ],
        code='(VARIABLE_0.strftime("%A"), VARIABLE_0.day)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            optional('the'),
            optional('current|present'),
            WEEKDAY,
            split('and|with'),
            optional('the'),
            split('day|days'),
            optional('tuple|in tuple|in a tuple')
        ],
        code='(VARIABLE_0.strftime("%A"), VARIABLE_0.day)'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            optional('the'),
            optional('current|present'),
            split('year|years'),
            split('and|with'),
            optional('the'),
            WEEKDAY,
            optional('tuple|in tuple|in a tuple')
        ],
        code='(VARIABLE_0.year, VARIABLE_0.strftime("%A"))'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('month|months'),
            optional('of'),
            DATE_OPT,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.month'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('day|days'),
            optional('of'),
            DATE_OPT,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.day'
    )
]

date_strftime_pairs = [
    Pair(
        description=[
            GET,
            optional('the'),
            split('short|small|smaller|shorter'),
            split('version|alternative'),
            ['of the'],
            WEEKDAY,
            DURING,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.strftime("%a")'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            WEEKDAY,
            DURING,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.strftime("%A")'
    ),

    Pair(
        description=[
            GET,
            optional('the'),
            split('current|todays|right nows|immediate|present'),
            WEEKDAY,
            DURING,
            optional('now')
        ],
        code='datetime.today().strftime("%A")'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            WEEKDAY,
            CURREN_OPT
        ],
        code='datetime.today().strftime("%A")'
    ),

    Pair(
        description=[
            GET,
            optional('the'),
            split('nth|number for the'),
            WEEKDAY,
            DURING,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.weekday()'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            ['iso'],
            split('nth|number for the'),
            WEEKDAY,
            DURING,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.isoweekday()'
    ),

    Pair(
        description=[
            GET,
            split('todays|the days|the current'),
            WEEKDAY
        ],
        code='datetime.today().weekday()'
    ),
    Pair(
        description=[
            GET,
            split('todays|the days|the current'),
            ['iso'],
            WEEKDAY
        ],
        code='datetime.today().isoweekday()'
    )
]


date_simple_mod_pairs = [
    Pair(
        description=[
            GET,
            split('tomorrow|tomorrows'),
            DATE
        ],
        code='datetime.date.today() + datetime.timedelta(days=1)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            DATE,
            split('tomorrow|tomorrows')
        ],
        code='datetime.date.today() + datetime.timedelta(days=1)'
    ),
    Pair(
        description=[
            GET,
            split('the day after|day after'),
            split('tomorrow|tomorrows'),
            DATE
        ],
        code='datetime.date.today() + datetime.timedelta(days=2)'
    ),

    Pair(
        description=[
            GET,
            split('yesterday|yesterdays'),
            DATE
        ],
        code='datetime.date.today() + datetime.timedelta(days=-1)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            DATE,
            split('yesterday|yesterdays')
        ],
        code='datetime.date.today() + datetime.timedelta(days=-1)'
    ),
    Pair(
        description=[
            GET,
            split('the day before|day before'),
            split('yesterday|yesterdays'),
            DATE
        ],
        code='datetime.date.today() + datetime.timedelta(days=-2)'
    ),

    Pair(
        description=[
            GET,
            ['NUMBER_0'],
            split('days|day'),
            split('after|in the future of|added to'),
            CURRENT
        ],
        code='datetime.date.today() + datetime.timedelta(days=NUMBER_0)'
    ),
    Pair(
        description=[
            GET,
            ['NUMBER_0'],
            split('days|day'),
            split('before|past'),
            CURRENT
        ],
        code='datetime.date.today() + datetime.timedelta(days=-NUMBER_0)'
    ),
    Pair(
        description=[
            split('add|append|amend'),
            ['NUMBER_0'],
            ['days to'],
            CURRENT
        ],
        code='datetime.date.today() + datetime.timedelta(days=NUMBER_0)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('date|dates|datetime date|datetime dates'),
            ['NUMBER_0'],
            split('days|day'),
            split('ago|before now|in the past|back|before')
        ],
        code='datetime.date.today() + datetime.timedelta(days=-NUMBER_0)'
    ),
    Pair(
        description=[
            SUBTRACT,
            ['VARIABLE_0'],
            split('days|day'),
            optional('to'),
            optional('the'),
            optional('datetime'),
            split('date|dates'),
        ],
        code='datetime.date.today() + datetime.timedelta(days=-VARIABLE_0)'
    ),
]

stack_overflow_pairs = [
    Pair(
        description=[
            optional('the'),
            CURREN_OPT,
            ['time']
        ],
        code='datetime.datetime.now().time()'
    ),
    Pair(
        description=[
            optional('create|make|instantiate'),
            optional('a'),
            optional('new'),
            DATETIME,
            split('from|using|with|via|through'),
            optional('a'),
            optional('utc'),
            split('timestamp VARIABLE_0|tstamp VARIABLE_0|time stamp VARIABLE_0|VARIABLE_0 timestamp|VARIABLE_0 tstamp|VARIABLE_0 time stamp'),
        ],
        code='datetime.datetime.utcfromtimestamp(VARIABLE_0)'
    ),
    Pair(
        description=[
            DATETIME,
            optional('made|created|create'),
            split('from|using|with|via|through'),
            DATE,
            ['VARIABLE_0']
        ],
        code='datetime.datetime.combine(VARIABLE_0, datetime.datetime.min.time())'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('passed'),
            split('since|past|after'),
            split('midnight|12 am|start of day|day begining')
        ],
        code='now = datetime.datetime.now()\nresult = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            split('to|until|before'),
            MIDNIGHT
        ],
        code='now = datetime.datetime.now()\nresult = (now.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1) - now).total_seconds()'
    ),

    Pair(
        description=[
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('passed'),
            split('since|past|after'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='now = datetime.datetime.now()\nresult = (now - VARIABLE_0).total_seconds()'
    ),
    Pair(
        description=[
            ['return'],
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('passed'),
            split('since|past|after'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='now = datetime.datetime.now()\nreturn (now - VARIABLE_0).total_seconds()'
    ),
    Pair(
        description=[
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            split('to|until|before'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='now = datetime.datetime.now()\nresult = (VARIABLE_0 - now).total_seconds()'
    ),
    Pair(
        description=[
            optional('the'),
            NUMBER,
            optional('of'),
            split('days|day'),
            split('to|until|before'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='now = datetime.datetime.now()\nresult = (VARIABLE_0 - now).days'
    ),
    Pair(
        description=[
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('since|past|after|between|in between|from'),
            DATE_OR_DATETIME_VAR_0,
            optional('to|until|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='(VARIABLE_1 - VARIABLE_0).total_seconds()'
    ),
    Pair(
        description=[
            optional('the'),
            NUMBER,
            optional('of'),
            split('days|day'),
            optional('passed|gone|been'),
            split('since|past|after|between|in between|from|by'),
            optional('the'),
            DATE_OR_DATETIME_VAR_0,
            optional('and'),
            CURRENT
        ],
        code='now = datetime.datetime.now()\nresult = (now - VARIABLE_0).days'
    ),
    Pair(
        description=[
            split('days|day'),
            split('since|past|after|between|in between|from|passed|gone|been'),
            CURRENT,
            optional('and'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='now = datetime.datetime.now()\nresult = (now - VARIABLE_0).days'
    )
]

fill_in_the_gap_pairs = [
    Pair(
        description=[
            NUMBER,
            optional('of'),
            split('days|day'),
            optional('since|past|after|between|in between|from'),
            DATE_OR_DATETIME_VAR_0,
            optional('to|until|and'),
            DATE_OR_DATETIME_VAR_1
        ],
        code='(VARIABLE_1 - VARIABLE_0).days'
    ),
    Pair(
        description=[
            split('difference|delta|distinction|divergence|gap|disparity|deviation'),
            optional('since|past|after|between|in between|from'),
            DATE_OR_DATETIME_VAR_0,
            optional('to|until|and'),
            DATE_OR_DATETIME_VAR_1,
            optional('in|as'),
            split('days|day')
        ],
        code='(VARIABLE_1 - VARIABLE_0).days'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('name|string'),
            optional('of the'),
            split('current|todays|right nows|immediate|present'),
            split('month|months')
        ],
        code='datetime.datetime.now().strftime("%B")'
    ),
    Pair(
        description=[
            split('current|todays|right nows|immediate|present'),
            split('month|months'),
            split('name|string')
        ],
        code='datetime.datetime.now().strftime("%B")'
    ),
    Pair(
        description=[
            split('month|months'),
            split('on|during|when|while|within|at|in'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.strftime("%B")'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('name|string'),
            optional('of the'),
            split('month|months'),
            split('on|during|when|while|within|at|in'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.strftime("%B")'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('since|past|after|between|in between|from'),
            DATE_OR_DATETIME_VAR_0,
            optional('to|until|before|from|and'),
            optional('the'),
            optional('next|coming'),
            MIDNIGHT
        ],
        code='(VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1) - VARIABLE_0).total_seconds()'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            DATETIME,
            optional('of'),
            optional('the'),
            optional('last|past'),
            MIDNIGHT,
            split('before|prior|past|passed|gone by|gone'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            DATETIME,
            optional('of'),
            optional('the'),
            optional('next|coming'),
            MIDNIGHT,
            split('after|comming|next|in the future of'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1)'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            split('seconds|second'),
            split('to|until|before|from'),
            optional('the'),
            optional('next|coming'),
            MIDNIGHT
        ],
        code='(VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1) - VARIABLE_0).total_seconds()'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('passed'),
            split('since|after|past|over|from|between'),
            optional('the'),
            optional('last|past'),
            MIDNIGHT,
            optional('and|to|until'),
            DATE_OR_DATETIME_VAR_0,
        ],
        code='(VARIABLE_0 - VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()'
    ),
    Pair(
        description=[
            DATE_OR_DATETIME_VAR_0,
            optional('the'),
            NUMBER,
            optional('of'),
            split('seconds|second'),
            optional('passed'),
            split('since|after|past|over|from|between'),
            optional('the'),
            optional('last|past'),
            MIDNIGHT
        ],
        code='(VARIABLE_0 - VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()'
    ),
    Pair(
        description=[
            NUMBER,
            optional('of'),
            split('seconds|second'),
            split('since|after|past|over|from|passed|is ahead of|ahead|ahead of'),
            optional('last|past'),
            MIDNIGHT,
            optional('on'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='(VARIABLE_0 - VARIABLE_0.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()'
    ),
    Pair(
        description=[
            split('using|with|via|through'),
            DATE,
            ['VARIABLE_0'],
            split('create|make|instantiate'),
            optional('a'),
            optional('new'),
            DATETIME
        ],
        code='datetime.datetime.combine(VARIABLE_0, datetime.datetime.min.time())'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            ['time'],
            optional('at|during|in|from|with|using|via|on|through'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.time()'
    ),
    Pair(
        description=[
            optional('the'),
            split('isoformat|iso format|format iso|iso'),
            optional('at|during|in|of'),
            DATE_OR_DATETIME_VAR_0
        ],
        code='VARIABLE_0.isoformat()'
    )
]


description_code_pairs = date_basic + date_testable_pairs + attribute_pairs + timedelta_pairs + date_attributes_pairs + date_strftime_pairs + date_simple_mod_pairs + stack_overflow_pairs + fill_in_the_gap_pairs
