from _annotation_tools import Pair, split, optional

OPEN_OPTIONS = split('load|open|read')


description_code_pairs = [
    Pair(
        description=[
            OPEN_OPTIONS,
            ['json'],
            optional('load'),
            optional('from'),
            ['VARAIBLE_0']
        ],
        code="json.load(VARAIBLE_0)",
        modules=['json']
    ),
    Pair(
        description=[
            OPEN_OPTIONS,
            ['json'],
            optional('load|open|read'),
            optional('from'),
            ['STRING_0']
        ],
        code="""with open(STRING_0, 'w') as outfile:
    obj = json.load(outfile)""",
        modules=['json']
    ),
    Pair(
        description=[
            split('dump|save'),
            ['VARAIBLE_0'],
            optional('as'),
            ['json'],
            optional('at'),
            ['STRING_0']
        ],
        code="""with open(STRING_0, 'w') as outfile:
    json.dump(VARAIBLE_0, outfile)""",
        modules=['json']
    )
]
