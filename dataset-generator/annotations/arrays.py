from _annotation_tools import Pair, split, optional

CHECK = optional('check|see|find|check that|check if|see if|find if|true if|true given|is')
GET = split('get|find|take')
EACH = optional('each|every|all|every single|each single|all of the|all the|each of the|every one of the|every single one of the')
ANY = split('any|a single|a|there exists a|there is a|there is at least one|for a|for a single')
ELEMENT_STR = 'element|value|member|elements|values|members'
NUMBER = split('number|int|item|unit|float|integer|numbers|ints|items|units|floats|integers|' + ELEMENT_STR)
BOOL = split('bool|boolean|' + ELEMENT_STR)
NUMBERS = split('numbers|ints|items|elements|values|units|floats|integers|members')
REMOVE = split('remove|del|delete|take away')
LARGEST = split('largest|greatest|biggest|max|highest|most large|most big|most high')
SMALLEST = split('smallest|lowest|tiniest|min|shortest|most small|most low|most tiny|most short')
IN = split('in|inside|inside of|within|contained by|contained in|from')
EQUAL = split('equal|same|identical|match|matching')
FIRST_STR = 'first|starting|initial|start|begining|initial'
FIRST = split(FIRST_STR)
LAST_STR = 'last|ending|final|end|ending|final|latter'
LAST = split(LAST_STR)

FOUR = split('four|4')
THREE = split('three|3')
TWO = split('two|2')

q1_pairs = [
    Pair(
        description=[
            split('double|two times|times two'),
            EACH,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='[2*v for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            SMALLEST,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.remove(min(VARIABLE_0))'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            LARGEST,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='max(VARIABLE_0)'
    ),
]

q2_pairs = [
    Pair(
        description=[
            REMOVE,
            optional('the'),
            LARGEST,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0.remove(max(VARIABLE_0))'
    )
]

q3_pairs = [
    Pair(
        description=[
            CHECK,
            ['VARIABLE_0'],
            optional('is'),
            optional('a'),
            ['palindrome']
        ],
        code='False not in [VARIABLE_0[i] == VARIABLE_0[-i - 1] for i in range(len(VARIABLE_0))]'
    ),
    Pair(
        description=[
            CHECK,
            EACH,
            NUMBER,
            IN,
            ['VARIABLE_0'],
            optional('is'),
            optional('the'),
            EQUAL,
            optional('to'),
            optional('its|itself'),
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped')
        ],
        code='False not in [VARIABLE_0[i] == VARIABLE_0[-i - 1] for i in range(len(VARIABLE_0))]'
    ),
    Pair(
        description=[
            CHECK,
            ['VARIABLE_0'],
            optional('is'),
            optional('the'),
            EQUAL,
            optional('both'),
            split('forward|forwards|in its current order|in regular order|normal order'),
            optional('and'),
            optional('its|itself'),
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped')
        ],
        code='False not in [VARIABLE_0[i] == VARIABLE_0[-i - 1] for i in range(len(VARIABLE_0))]'
    ),
    Pair(
        description=[
            CHECK,
            ['VARIABLE_0'],
            optional('is'),
            EQUAL,
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped')
        ],
        code='False not in [VARIABLE_0[i] == VARIABLE_0[-i - 1] for i in range(len(VARIABLE_0))]'
    ),
    Pair(
        description=[
            CHECK,
            optional('both'),
            split('forward|forwards|in its current order|in regular order|normal order'),
            optional('and'),
            optional('its|itself'),
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped'),
            optional('in'),
            ['VARIABLE_0'],
            optional('is'),
            optional('the'),
            EQUAL,
        ],
        code='False not in [VARIABLE_0[i] == VARIABLE_0[-i - 1] for i in range(len(VARIABLE_0))]'
    ),
    Pair(
        description=[
            CHECK,
            EACH,
            NUMBER,
            IN,
            ['VARIABLE_0'],
            optional('is'),
            split('even|divisable by two|halvable|can be halved')
        ],
        code='False not in [v % 2 == 0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            split('does|has'),
            ['VARIABLE_0'],
            split('just|only'),
            optional('got|contains|holds|hold|contain'),
            split('even|divisable by two|halvable|can be halved'),
            NUMBER
        ],
        code='False not in [v % 2 == 0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            CHECK,
            EACH,
            NUMBER,
            IN,
            ['VARIABLE_0'],
            optional('is|can be|could be'),
            split('divisable|divided'),
            optional('by|using|with'),
            ['NUMBER_0']
        ],
        code='False not in [v % NUMBER_0 == 0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            split('sort|order|reorder|sorted|arrange|rearrange'),
            ['VARIABLE_0'],
            optional('by|using|with'),
            optional('the'),
            split('length|size|len|number of characters|amount of chars'),
            optional('all'),
            optional('of'),
            split('each|every|its|it s'),
            split('string|phrase|strings|phrases')
        ],
        code='VARIABLE_0.sort(key = lambda s: len(s))'
    )
]

slicing_pairs = [
    Pair(
        description=[
            REMOVE,
            optional('the'),
            FIRST,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[1:]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            FIRST,
            NUMBER,
            IN,
            ['LIST_0']
        ],
        code='LIST_0[1:]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            FIRST,
            ['NUMBER_0'],
            NUMBERS,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[NUMBER_0:]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            FIRST,
            NUMBERS,
            IN,
            ['LIST_0']
        ],
        code='LIST_0[NUMBER_0:]'
    ),

    Pair(
        description=[
            REMOVE,
            optional('the'),
            LAST,
            NUMBER,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[:-1]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            LAST,
            NUMBER,
            IN,
            ['LIST_0']
        ],
        code='LIST_0[:-1]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            LAST,
            ['NUMBER_0'],
            NUMBERS,
            IN,
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[:-NUMBER_0]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            LAST,
            NUMBERS,
            IN,
            ['LIST_0']
        ],
        code='LIST_0[:-NUMBER_0]'
    )
]

std_py_functions = [
    Pair(
        description=[
            CHECK,
            EACH,
            BOOL,
            IN,
            ['VARIABLE_0'],
            split('is|has value|is set to'),
            split('true|True|positive|not empty|non empty')
        ],
        code='all(VARIABLE_0)'
    ),
    Pair(
        description=[
            CHECK,
            ANY,
            BOOL,
            IN,
            ['VARIABLE_0'],
            split('is|has value|is set to'),
            split('true|True|positive|not empty|non empty')
        ],
        code='any(VARIABLE_0)'
    )
]

'''
Taken from:     https://stackoverflow.com/search?q=%5Bpython%5D+%5Blist%5D+views%3A10000+answers%3A4+closed%3Ano+duplicate%3Ano+migrated%3Ano+wiki%3Ano+is%3Aquestion
Start:          10:16
End:            11:16
Count:          30 pairs from looking at 48 Stack Overflow Questions
'''
stack_overflow_pairs = [
    Pair(
        description=[
            CHECK,
            ['LIST_0'],
            optional('is'),
            ['empty']
        ],
        code='if LIST_0:'
    ),
    Pair(
        description=[
            ['find'],
            split('index|position|place|pos'),
            split('of|where value'),
            ['VARIABLE_0'],
            split('in|inside of|contained in'),
            ['LIST_0']
        ],
        code='LIST_0.index(VARIABLE_0)'
    ),
    Pair(
        description=[
            ['find'],
            split('index|position|place|pos'),
            split('of|where value'),
            ['NUMBER_0'],
            split('in|inside of|contained in'),
            ['LIST_0']
        ],
        code='LIST_0.index(NUMBER_0)'
    ),
    Pair(
        description=[
            ['find'],
            split('index|position|place|pos'),
            split('of|where value'),
            ['STRING_0'],
            split('in|inside of|contained in'),
            ['LIST_0']
        ],
        code='LIST_0.index(STRING_0)'
    ),
    Pair(
        description=[
            optional('do|do a'),
            split('for loop|for|loop|loop through'),
            ['LIST_0'],
            split('getting|keeping'),
            optional('the'),
            split('index|position|place')
        ],
        code='for index, value in enumerate(LIST_0):'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split(LAST_STR + '|second'),
            split('half|halves|2|two|split|slice|section|part'),
            optional('of|from'),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[len(VARIABLE_0)//2:]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split(FIRST_STR),
            split('half|halves|2|two|split|slice|section|part'),
            optional('of|from'),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[:len(VARIABLE_0)//2]'
    ),
    Pair(
        description=[
            split('make|create'),
            optional('a'),
            ['flat'],
            split('list|array'),
            split('from|out of'),
            split('list|array'),
            ['of'],
            split('lists|arrays'),
            ['LIST_0']
        ],
        code='[item for sublist in LIST_0 for item in sublist]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('number|count|amount'),
            ['of'],
            NUMBERS,
            ['in'],
            ['LIST_0']
        ],
        code='len(LIST_0)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            split('len|length|size'),
            optional('of'),
            ['LIST_0']
        ],
        code='len(LIST_0)'
    ),
    Pair(
        description=[
            split('clone|copy|duplicate|recreate|remake'),
            ['LIST_0']
        ],
        code='LIST_0.copy()'
    ),
    Pair(
        description=[
            ['LIST_0'],
            split('clone|copy|duplicate|recreate|remake')
        ],
        code='LIST_0.copy()'
    ),
    Pair(
        description=[
            split('split|cut|slice|chop'),
            ['VARIABLE_0'],
            split('into|to'),
            split('evenly|same'),
            split('sized|lengthed'),
            split('chunks|sections|lists'),
            ['NUMBER_0']
        ],
        code='[VARIABLE_0[i:i + NUMBER_0] for i in range(0, len(VARIABLE_0), NUMBER_0)]'
    ),
    Pair(
        description=[
            split('join|merge|combine'),
            optional('items|elements|members|strings'),
            optional('in'),
            ['LIST_0'],
            optional('with|using'),
            ['STRING_0']
        ],
        code='STRING_0.join(LIST_0)'
    ),
    Pair(
        description=[
            optional('find the'),
            split('count|number'),
            optional('of'),
            ['occurences'],
            optional('of'),
            ['NUMBER_0'],
            split('in|within|inside'),
            ['LIST_0']
        ],
        code='LIST_0.count(NUMBER_0)'
    ),
    Pair(
        description=[
            REMOVE,
            ['NUMBER_0'],
            split('from|in|inside|contained in'),
            ['LIST_0']
        ],
        code='LIST_0.remove(NUMBER_0)'
    ),
    Pair(
        description=[
            optional('find the'),
            split('count|number'),
            optional('of'),
            ['occurences'],
            optional('of'),
            ['STRING_0'],
            split('in|within|inside'),
            ['LIST_0']
        ],
        code='LIST_0.count(STRING_0)'
    ),
    Pair(
        description=[
            REMOVE,
            ['STRING_0'],
            split('from|in|inside|contained in'),
            ['LIST_0']
        ],
        code='LIST_0.remove(STRING_0)'
    ),
    Pair(
        description=[
            REMOVE,
            split('duplicates|copies|same values|identical members'),
            optional('from|in|inside|contained in'),
            ['LIST_0'],
            split('preserving|maintaining|keeping'),
            optional('the'),
            split('order|ordering')
        ],
        code='list(collections.OrderedDict.fromkeys(LIST_0))'
    ),
    Pair(
        description=[
            REMOVE,
            split('duplicates|copies|same values|identical members'),
            optional('from|in|inside|contained in'),
            ['LIST_0']
        ],
        code='list(set(LIST_0))'
    ),
    Pair(
        description=[
            split('check|see|show'),
            optional('if'),
            ['NUMBER_0'],
            split('is|contained|held|hold'),
            split('in|inside|within'),
            ['LIST_0']
        ],
        code='NUMBER_0 in LIST_0'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            FIRST,
            NUMBER,
            split('in|inside|within'),
            ['LIST_0']
        ],
        code='del LIST_0[0]'
    ),
    Pair(
        description=[
            REMOVE,
            split('empty|0 lengthed|zero lengthed'),
            split('strings|phrases'),
            split('in|inside|within'),
            ['LIST_0']
        ],
        code='list(filter(None, LIST_0))'
    ),
    Pair(
        description=[
            split('split|slice|chop|cut'),
            split('string|phrase'),
            ['STRING_0'],
            optional('into a list|to a list')
        ],
        code='STRING_0.split()'
    ),
    Pair(
        description=[
            split('split|slice|chop|cut'),
            split('string|phrase'),
            ['STRING_0'],
            optional('on'),
            ['STRING_1']
        ],
        code='STRING_0.split(STRING_1)'
    ),
    Pair(
        description=[
            split('make|create'),
            optional('a'),
            ['comma'],
            split('separated|deliminated|split'),
            ['string'],
            split('from|using'),
            ['LIST_0']
        ],
        code="','.join(LIST_0)"
    ),
    Pair(
        description=[
            split('make|create'),
            optional('a'),
            ['space'],
            split('separated|deliminated|split'),
            ['string'],
            split('from|using'),
            ['LIST_0']
        ],
        code="' '.join(LIST_0)"
    ),
    Pair(
        description=[
            split('make|create'),
            optional('a'),
            ['STRING_0'],
            split('separated|deliminated|split'),
            ['string'],
            split('from|using'),
            ['LIST_0']
        ],
        code="STRING_0.join(LIST_0)"
    ),
    Pair(
        description=[
            GET,
            split('difference|difference between|delta|elements not in either|items not a part of|members only found in one of'),
            ['LIST_0'],
            ['LIST_1']
        ],
        code="list(set(LIST_0) - set(LIST_1))"
    ),
    Pair(
        description=[
            GET,
            split('same|same between|identical members|elements in both|items a part of|values found in each of'),
            ['LIST_0'],
            ['LIST_1']
        ],
        code="list(set(LIST_0) & set(LIST_1))"
    ),
    Pair(
        description=[
            split('append|add'),
            ['STRING_0'],
            optional('to'),
            ['LIST_0']
        ],
        code="LIST_0.append(STRING_0)"
    ),
    Pair(
        description=[
            split('prepend|add to end of| add to end'),
            ['STRING_0'],
            ['LIST_0']
        ],
        code="LIST_0.insert(0, STRING_0)"
    ),
    Pair(
        description=[
            split('empty|clear'),
            ['LIST_0']
        ],
        code="LIST_0.clear()"
    ),
    Pair(
        description=[
            REMOVE,
            EACH,
            NUMBERS,
            ['LIST_0']
        ],
        code="LIST_0.clear()"
    )
]

description_code_pairs = (
    q1_pairs + q2_pairs + q3_pairs + slicing_pairs + std_py_functions + stack_overflow_pairs
)
