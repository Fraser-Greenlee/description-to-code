from _annotation_tools import Pair, split, optional

FILE_SPLIT = 'file|script'
IF_OPTIONS = split('if|given')
ORDER = optional('order|reorder|rearrange|arrange|organise|reorganise')
GET = split('get|find|take')
REMOVE = split('remove|delete|get rid of|take away|clear')
FIRST = split('1st|starting|first|initial|start|beginning')
LAST = split('ending|last|final')
ELEMENT = 'item|value|element|items|values|elements|member|members'
SPL_ELEMENT = split(ELEMENT)
OPT_ELEMENT = optional(ELEMENT)
ITEMS = split('elements|components|items|members|values|element|component|item|member|value')
TIMES = split('times|multiply|timesed by|multiplied by|times using|multiplied with')
SUM = split('sum|add|combine|join|plus|addition')
SUBTRACT = split('subtract|difference|difference between|amount between')
EVERY = optional('every|each|all')
INDEX = split('index|position')
MAX = split('max|maximum|largest|biggest|greatest|highest|most large')
MIN = split('min|minimum|smalest|littlest|lowest value')


description_code_pairs = [

    Pair(
        description=[
            IF_OPTIONS,
            ['name'],
            optional('is'),
            split('main|running|{file}'.format(file=FILE_SPLIT))
        ],
        code='''if __name__ == '__main__':
    '''
    ),
    Pair(
        description=[
            IF_OPTIONS,
            split(FILE_SPLIT),
            optional('is'),
            ['running']
        ],
        code='''if __name__ == '__main__':
    '''
    ),

    Pair(
        description=[
            ORDER,
            ['LIST_0'],
            optional('in'),
            split('asc|ascending|increasing'),
            ORDER
        ],
        code='sorted(LIST_0)'
    ),
    Pair(
        description=[
            ORDER,
            ['VARIABLE_0'],
            optional('in'),
            split('asc|ascending|increasing'),
            ORDER
        ],
        code='sorted(VARIABLE_0)'
    ),
    Pair(
        description=[
            ORDER,
            ['LIST_0'],
            optional('in'),
            split('desc|descending|decreasing'),
            ORDER
        ],
        code='sorted(LIST_0, reverse=True)'
    ),
    Pair(
        description=[
            ORDER,
            ['VARIABLE_0'],
            optional('in'),
            split('desc|descending|decreasing'),
            ORDER
        ],
        code='sorted(VARIABLE_0, reverse=True)'
    ),
    Pair(
        description=[
            optional('in'),
            ORDER,
            split('asc|ascending|increasing'),
            ['LIST_0'],
        ],
        code='sorted(LIST_0)'
    ),
    Pair(
        description=[
            optional('in'),
            ORDER,
            split('asc|ascending|increasing'),
            ['VARIABLE_0'],
        ],
        code='sorted(VARIABLE_0)'
    ),
    Pair(
        description=[
            optional('in'),
            ORDER,
            split('desc|descending|decreasing'),
            ['LIST_0'],
        ],
        code='sorted(LIST_0, reverse=True)'
    ),
    Pair(
        description=[
            optional('in'),
            ORDER,
            split('desc|descending|decreasing'),
            ['VARIABLE_0'],
        ],
        code='sorted(VARIABLE_0, reverse=True)'
    ),

    Pair(
        description=[
            GET,
            optional('the'),
            FIRST,
            SPL_ELEMENT,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[0]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            LAST,
            SPL_ELEMENT,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[-1]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            SPL_ELEMENT,
            optional('at'),
            INDEX,
            ['NUMBER_0'],
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[NUMBER_0]'
    ),
    Pair(
        description=[
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped'),
            ['VARIABLE_0']
        ],
        code='VARIABLE_0[::-1]'
    ),
    Pair(
        description=[
            ['VARIABLE_0'],
            optional('in'),
            split('backward|backwards|reverse|reversed|mirror|mirrored|opposite|flip|flipped')
        ],
        code='VARIABLE_0[::-1]'
    ),
    Pair(
        description=[
            GET,
            ITEMS,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0'],
            ['after'],
            INDEX,
            ['NUMBER_0']
        ],
        code='VARIABLE_0[NUMBER_0 + 1:]'
    ),
    Pair(
        description=[
            GET,
            ITEMS,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0'],
            ['before'],
            optional('index'),
            ['NUMBER_0']
        ],
        code='VARIABLE_0[:NUMBER_0]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            MAX,
            OPT_ELEMENT,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='max(VARIABLE_0)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            MIN,
            OPT_ELEMENT,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='min(VARIABLE_0)'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            ['NUMBER_0'],
            MAX,
            ITEMS,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='sorted(VARIABLE_0, reverse=True)[:NUMBER_0]'
    ),
    Pair(
        description=[
            GET,
            optional('the'),
            ['NUMBER_0'],
            MIN,
            ITEMS,
            optional('of|from|in|within|inside'),
            ['VARIABLE_0']
        ],
        code='sorted(VARIABLE_0)[:NUMBER_0]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            ['NUMBER_0'],
            MAX,
            ITEMS,
            optional('in|from|contained in|inside'),
            ['VARIABLE_0']
        ],
        code='sorted(VARIABLE_0, reverse=True)[NUMBER_0:]'
    ),
    Pair(
        description=[
            REMOVE,
            optional('the'),
            ['NUMBER_0'],
            MIN,
            ITEMS,
            optional('in|from|contained in|inside'),
            ['VARIABLE_0']
        ],
        code='sorted(VARIABLE_0)[NUMBER_0:]'
    ),
    Pair(
        description=[
            optional('in|from|contained in|inside'),
            ['VARIABLE_0'],
            REMOVE,
            optional('the'),
            ['NUMBER_0'],
            MAX,
            ITEMS
        ],
        code='sorted(VARIABLE_0, reverse=True)[NUMBER_0:]'
    ),
    Pair(
        description=[
            optional('in|from|contained in|inside'),
            ['VARIABLE_0'],
            REMOVE,
            optional('the'),
            ['NUMBER_0'],
            MIN,
            ITEMS
        ],
        code='sorted(VARIABLE_0)[NUMBER_0:]'
    ),
    Pair(
        description=[
            REMOVE,
            SPL_ELEMENT,
            optional('at index'),
            ['NUMBER_0'],
            optional('from'),
            ['VARIABLE_0']
        ],
        code='del VARIABLE_0[NUMBER_0]'
    ),
    Pair(
        description=[
            ['square'],
            ['NUMBER_0']
        ],
        code='NUMBER_0**2'
    ),
    Pair(
        description=[
            ['square'],
            ['NUMBER_0']
        ],
        code='NUMBER_0**2'
    ),
    Pair(
        description=[
            ['NUMBER_0'],
            ['squared']
        ],
        code='NUMBER_0**2'
    ),
    Pair(
        description=[
            ['NUMBER_0'],
            optional('to the'),
            ['power'],
            optional('of|from|in|within|inside'),
            ['NUMBER_1']
        ],
        code='NUMBER_0**NUMBER_1'
    ),
    Pair(
        description=[
            split('double|2x|2 times'),
            ['NUMBER_0']
        ],
        code='NUMBER_0*2'
    ),
    Pair(
        description=[
            ['triple'],
            split('triple|3x|3 times'),
            ['NUMBER_0']
        ],
        code='NUMBER_0*3'
    ),
    Pair(
        description=[
            ['half'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/2'
    ),
    Pair(
        description=[
            ['third'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/3'
    ),
    Pair(
        description=[
            ['quarter'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/4'
    ),
    Pair(
        description=[
            ['fourth'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/4'
    ),
    Pair(
        description=[
            ['fith'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/5'
    ),
    Pair(
        description=[
            ['sixth'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/6'
    ),
    Pair(
        description=[
            ['seventh'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/7'
    ),
    Pair(
        description=[
            ['eigth'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/8'
    ),
    Pair(
        description=[
            ['nineth'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/9'
    ),
    Pair(
        description=[
            ['tenth'],
            ['NUMBER_0']
        ],
        code='NUMBER_0/10'
    ),
    Pair(
        description=[
            ['divide NUMBER_0'],
            split('by|using|with'),
            ['NUMBER_1']
        ],
        code='NUMBER_0/NUMBER_1'
    ),
    Pair(
        description=[
            ['floor divide NUMBER_0'],
            split('by|using|with'),
            ['NUMBER_1']
        ],
        code='NUMBER_0//NUMBER_1'
    ),
    Pair(
        description=[
            ['NUMBER_0'],
            split('over|divided by|divides|divided with'),
            ['NUMBER_1']
        ],
        code='NUMBER_0/NUMBER_1'
    ),
    Pair(
        description=[
            GET,
            ['remainder'],
            optional('for'),
            ['NUMBER_0'],
            split('over|divided by'),
            ['NUMBER_1']
        ],
        code='NUMBER_0 % NUMBER_1'
    ),
    Pair(
        description=[
            ['NUMBER_0'],
            TIMES,
            ['NUMBER_1']
        ],
        code='NUMBER_0*NUMBER_1'
    ),


    Pair(
        description=[
            ['double'],
            EVERY,
            SPL_ELEMENT,
            split('of|in'),
            ['LIST_0']
        ],
        code='[v*2 for v in LIST_0]'
    ),
    Pair(
        description=[
            ['half'],
            EVERY,
            SPL_ELEMENT,
            split('of|in'),
            ['LIST_0']
        ],
        code='[v/2 for v in LIST_0]'
    ),
    Pair(
        description=[
            ['triple'],
            EVERY,
            SPL_ELEMENT,
            split('of|in'),
            ['LIST_0']
        ],
        code='[v*3 for v in LIST_0]'
    ),
    Pair(
        description=[
            ['square'],
            EVERY,
            SPL_ELEMENT,
            split('of|in'),
            ['LIST_0']
        ],
        code='[v**2 for v in LIST_0]'
    ),
    Pair(
        description=[
            split('set|assign'),
            EVERY,
            SPL_ELEMENT,
            optional('of|in'),
            ['LIST_0'],
            optional('to'),
            optional('the'),
            split('power of NUMBER_0|NUMBER_0 power|NUMBER_0 of power|NUMBER_0 th power|power NUMBER_0')
        ],
        code='[v**NUMBER_0 for v in LIST_0]'
    ),
    Pair(
        description=[
            split('set|assign'),
            EVERY,
            SPL_ELEMENT,
            optional('of|in'),
            ['VARIABLE_0'],
            optional('to'),
            optional('the'),
            split('power of VARIABLE_1|VARIABLE_1 power|VARIABLE_1 of power|VARIABLE_1 th power|power VARIABLE_1')
        ],
        code='[v**VARIABLE_1 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            split('set|assign'),
            optional('to'),
            optional('the'),
            split('power of VARIABLE_0|VARIABLE_0 power|VARIABLE_0 of power|VARIABLE_0 th power|power VARIABLE_0'),
            EVERY,
            SPL_ELEMENT,
            optional('of|in'),
            ['VARIABLE_1'],
        ],
        code='[v**VARIABLE_0 for v in VARIABLE_1]'
    ),
    Pair(
        description=[
            TIMES,
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['LIST_0'],
            optional('by|with|using'),
            ['NUMBER_0']
        ],
        code='[v*NUMBER_0 for v in LIST_0]'
    ),
    Pair(
        description=[
            TIMES,
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_0'],
            optional('by|with|using'),
            ['NUMBER_0']
        ],
        code='[v*NUMBER_0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            SUM,
            ['NUMBER_0'],
            optional('to'),
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_0']
        ],
        code='[v+NUMBER_0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            SUBTRACT,
            ['NUMBER_0'],
            optional('to'),
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_0']
        ],
        code='[v-NUMBER_0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            TIMES,
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_0'],
            optional('by|with|using'),
            ['VARIABLE_1']
        ],
        code='[v*VARIABLE_1 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            optional('by|with|using'),
            ['NUMBER_0'],
            TIMES,
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_0']
        ],
        code='[v*NUMBER_0 for v in VARIABLE_0]'
    ),
    Pair(
        description=[
            optional('by|with|using'),
            ['VARIABLE_0'],
            TIMES,
            split('each|every|all'),
            SPL_ELEMENT,
            split('of|in|within|inside|inside of'),
            ['VARIABLE_1']
        ],
        code='[v*VARIABLE_0 for v in VARIABLE_1]'
    ),
    Pair(
        description=[
            GET,
            ['VARIABLE_0'],
            ['type']
        ],
        code='type(VARIABLE_0)'
    )
]
