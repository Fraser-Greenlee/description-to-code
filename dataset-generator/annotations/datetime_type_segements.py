'''
Demo for what a new Pairs system could do.

Should be able to define thesaurus for words e.g. end->ending

Then these should propagete betweem splits e.g. split('midnight|day end') -> split('midnight|day end|day ending')

The key concept is that I should only have to define something once.

Stop words should be automatically optional e.g. "the"

'''
import datetime

from _annotation_tools import Pair, split, optional

THESAURUS = [
    'date time|datetime|date and time|time and date',
    'day|days',
    'delta|difference',
    'timedelta|time delta|delta time|delta in time',
    'end|ending',
    'day|days',
    'this|the',
    'midnight|night|tonight|this night|day end|end day|night',
    'after|given|on|in',
    'plus|add|added to|add on|sum|addition',
    'minus|subtract|removed from|take away'
    'tomorrow|tomorrows|the next day',
    'now|nows',
    'right now|present',
    'current|immediate',
    'single|one',
    'week|weeks',
    'number|amount|count|occurences',
    'seconds|second'
]

THE = optional('the')

basic = [
    Pair(
        description='midnight',
        code='datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)'
    ),
    Pair(
        description=[
            [
                'midnight',
                optional('after'),
                '<datetime.datetime>_0'
            ],
            [
                '<datetime.datetime>_0',
                split('midnight')
            ],
            [
                'end',
                optional('of'),
                '<datetime.datetime>_0'
            ],
            [
                THE,
                '<datetime.datetime>_0',
                'end'
            ]
        ],
        code='<datetime.datetime>_0.replace(hour=0, minute=0, second=0, microsecond=0)'
    ),
    Pair(
        description=[
            'tomorrow'
        ],
        code='datetime.timedelta(days=1)',
        fragment_only=True
    ),
    Pair(
        description=[
            'tomorrows datetime'
        ],
        any_description_order=True,
        code='datetime.datetime.now() + datetime.timedelta(days=1)',
    ),
    Pair(
        description=[
            THE,
            '<datetime.timedelta>_0',
            '<datetime.datetime|datetime.date>_0'
        ],
        any_description_order=True,
        code='<datetime.datetime|datetime.date>_0 + <datetime.timedelta>_0'
    ),
    Pair(
        description=[
            [
                THE,
                optional('todays|present|current'),
                'datetime'
            ],
            [
                THE,
                'present'
            ]
        ],
        code='datetime.datetime.now()'
    ),
    Pair(
        description=[
            [
                optional('just'),
                THE,
                optional('today|present|current|calendar'),
                split('date|day'),
                optional('only')
            ],
            [
                'today'
            ]
        ],
        code='datetime.date.today()'
    ),
    Pair(
        description=[
            [
                'after',
                optional('timedelta'),
                '<int>_0',
                'days'
            ],
            [
                'after',
                'days',
                '<int>_0',
                optional('timedelta')
            ],
            [
                optional('new|make a'),
                'timedelta',
                '<int>_0',
                'days'
            ],
            [
                optional('new|make a'),
                'timedelta',
                'days',
                '<int>_0'
            ]
        ],
        code='datetime.timedelta(days=<int>_0)'
    ),
    Pair(
        description=[
            [
                'after',
                '<int>_0',
                'weeks'
            ],
            [
                'after',
                'weeks'
                '<int>_0'
            ]
        ],
        code='datetime.timedelta(days=7*<int>_0)'
    ),
    Pair(
        description=[
            [
                optional('a'),
                optional('single'),
                'day'
            ]
        ],
        code='datetime.timedelta(days=1)'
    ),
    Pair(
        description=[
            [
                optional('just'),
                optional('a'),
                optional('single'),
                'week'
            ],
            [
                'next',
                'week'
            ],
        ],
        code='datetime.timedelta(days=7)'
    ),
    Pair(
        description=[
            [
                optional('just'),
                optional('a'),
                optional('single'),
                'week',
                split('back|before')
            ],
            [
                'last',
                'week'
            ],
            [
                'week',
                'ago'
            ]
        ],
        code='datetime.timedelta(days=-7)'
    ),
    Pair(  # Would invoke a method for combining parameters in the description
        description=[
            [
                '<datetime.timedelta>_0',
                split('and|add'),
                '<datetime.timedelta>_1'
            ]
        ],
        code='$sum_params(<datetime.timedelta>_0,<datetime.timedelta>_1)'
    ),
    Pair(
        description=[
            [
                '<datetime.timedelta>_0',
                split('and|add'),
                '<datetime.timedelta>_1',
                split('and|add'),
                '<datetime.timedelta>_2'
            ]
        ],
        code='$sum_params(<datetime.timedelta>_0,<datetime.timedelta>_1,<datetime.timedelta>_2)'
    ),
    Pair(
        description=[
            [
                '<datetime.timedelta>_0',
                split('and|add'),
                '<datetime.timedelta>_1',
                split('and|add'),
                '<datetime.timedelta>_2',
                split('and|add'),
                '<datetime.timedelta>_3'
            ]
        ],
        code='$sum_params(<datetime.timedelta>_0,<datetime.timedelta>_1,<datetime.timedelta>_2,<datetime.timedelta>_3)'
    ),
    Pair(
        description=[
            [
                '<datetime.timedelta>_0',
                split('after|passed|plus'),
                '<datetime.datetime>_0'
            ],
            [
                '<datetime.datetime>_0',
                split('after|passed|plus'),
                '<datetime.timedelta>_0'
            ]
        ],
        code='<datetime.datetime>_0 + <datetime.timedelta>_0'
    ),
    Pair(
        description=[
            [
                '<datetime.timedelta>_0',
                split('before|minus|off'),
                '<datetime.datetime>_0'
            ]
        ],
        code='<datetime.datetime>_0 - <datetime.timedelta>_0'
    ),
    Pair(
        description=[
            'number',
            optional('of'),
            'seconds',
            'after'
            '<datetime.timedelta>_0'
        ],
        code='<datetime.timedelta>_0.total_seconds()'
    ),
    Pair(
        description=[
            split('from|between'),
            '<datetime.datetime>_0',
            optional('and|to'),
            '<datetime.datetime>_1'
        ],
        code='<datetime.datetime>_1 - <datetime.datetime>_0'
    ),
]
