
# Description to Code

This repo is for my 4th Year Individual Project.

## Running the Project

Start by restoring the mongoDB database.

Run: `mongorestore --db description_to_code mongodb_dump/description_to_code`

Then Setup and run projects in the following order:
1. dataset-generator
2. description-to-code-vscode-extension
3. description-to-code-server

Make sure you have the database, server and extension running at once to use the extension.

## mongodb_dump/

MongoDB database for the project, includes all data collected.

## description-to-code-vscode-extension/

Code for the VSCode editor extension.

## description-to-code-server/

Code for the server and deep learning model.

## programming_challenge

The programming challenge (user study) open this when running the VSCode editor extension to try it out.

## research/

Code for experiments with writing descriptions and trying to make a model.

